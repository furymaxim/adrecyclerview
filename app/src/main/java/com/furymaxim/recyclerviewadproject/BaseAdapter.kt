package com.furymaxim.recyclerviewadproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class BaseAdapter<T>(private val adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>): RecyclerView.Adapter<RecyclerView.ViewHolder>(), OnAdListener {

    private var visibleItemPosForAd: Int? = null
    private var isInsertionNeeded = true
    private var isDeleteNeeded = false
    var items: ArrayList<T?>?= null
    set(value) {
        if(value != null && value.isNotEmpty()) field = value
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        if(viewType == LAYOUT_BASE){
            return adapter.onCreateViewHolder(parent,viewType)
        }else{
            return AdViewHolder(inflater.inflate(R.layout.recycler_view_ad_item, parent, false))
        }
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder.itemViewType  == LAYOUT_BASE){
            if(position != visibleItemPosForAd) {
                adapter.onBindViewHolder(holder, position)
            }
        }else{
            val adHolder = holder as AdViewHolder
        }
    }

    private fun setVisibleItemPos(position: Int?){
        visibleItemPosForAd = position!!
    }

    fun getCurrentAdPos():Int?{
        return visibleItemPosForAd
    }

    override fun getItemViewType(position: Int): Int {

        if(position != visibleItemPosForAd){
            return LAYOUT_BASE
        }else{
            return LAYOUT_AD
        }

    }

    class AdViewHolder(view: View): RecyclerView.ViewHolder(view)

    override fun showAd(firstVisibleItemPosition: Int) {
        if(isInsertionNeeded){
            if(items == null) throw AdapterItemsAreNullException("Items are null. Probably, you forgot to set items.")
            items!!.add(firstVisibleItemPosition + 1, null)
            setVisibleItemPos(firstVisibleItemPosition + 1)
            notifyItemInserted(firstVisibleItemPosition + 1)
            isInsertionNeeded = false
            isDeleteNeeded = true
        }
    }

    override fun hideAd() {
        if(isDeleteNeeded == true) {
            if(items == null) throw AdapterItemsAreNullException("Items are null. Probably, you forgot to set items.")
            items!!.removeAt(visibleItemPosForAd!!)
            notifyItemRemoved(visibleItemPosForAd!!)
            setVisibleItemPos(-1)
            isInsertionNeeded = true
            isDeleteNeeded = false
        }
    }

    companion object{
        private const val LAYOUT_BASE = 0
        private const val LAYOUT_AD = 1
    }
}