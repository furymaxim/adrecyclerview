package com.furymaxim.recyclerviewadproject.adapters


import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.recyclerviewadproject.R
import com.furymaxim.recyclerviewadproject.models.Item
import java.lang.reflect.Array


class RecyclerViewAdapter(private var items: ArrayList<Item?>, private val context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val inflater = LayoutInflater.from(parent.context)

        return BaseViewHolder(inflater.inflate(R.layout.recycler_view_item, parent, false))
    }

    override fun getItemCount(): Int {
       return items.size
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        try {
            val myHolder = holder as BaseViewHolder

            myHolder.dateTV.text = items[position]!!.date
            myHolder.imageView.setImageDrawable(context!!.getDrawable(items[position]!!.image))
            myHolder.likeTV.text = items[position]!!.likeCount.toString()
        }catch (e: KotlinNullPointerException){
            Log.d("myLogs",e.printStackTrace().toString())
        }
    }


    class BaseViewHolder (view: View): RecyclerView.ViewHolder(view) {
        val imageView = view.findViewById<ImageView>(R.id.imageView)!!
        //val descriptionTV = view.findViewById<TextView>(R.id.description)
        val likeTV = view.findViewById<TextView>(R.id.likeCount)!!
        val dateTV = view.findViewById<TextView>(R.id.date)!!
    }

}


