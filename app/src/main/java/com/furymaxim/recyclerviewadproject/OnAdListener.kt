package com.furymaxim.recyclerviewadproject



interface OnAdListener {

    fun showAd(firstVisibleItemPosition: Int)

    fun hideAd()
}