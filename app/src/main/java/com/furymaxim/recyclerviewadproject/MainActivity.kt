package com.furymaxim.recyclerviewadproject

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.AbsListView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.recyclerviewadproject.adapters.RecyclerViewAdapter
import com.furymaxim.recyclerviewadproject.models.Item
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(){

    private var adapter: RecyclerViewAdapter? = null
    private var layoutManager: LinearLayoutManagerWrapper? = null

    private var isFirst = false
    private var isFinallyStopped = false



    private val items: ArrayList<Item?>?= ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addItems()

        layoutManager = LinearLayoutManagerWrapper(applicationContext)

        adapter = RecyclerViewAdapter(items!!, applicationContext)

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = BaseAdapter<Item>(adapter!!)

        (recyclerView.adapter as BaseAdapter<Item>).items = items

        recyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener(){

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE){

                    //if(isFirst == false) {

                        isFinallyStopped = true

                        Handler().postDelayed({
                            if(isFinallyStopped == true) {

                                if(layoutManager!!.findLastCompletelyVisibleItemPosition() != items.lastIndex && layoutManager!!.findLastCompletelyVisibleItemPosition() > ITEMS_VISIBLE_PER_PAGE - 1) {

                                    (recyclerView.adapter as BaseAdapter<Item>).showAd(layoutManager!!.findFirstVisibleItemPosition())
                                }else if(layoutManager!!.findLastCompletelyVisibleItemPosition() <= ITEMS_VISIBLE_PER_PAGE - 1){
                                    (recyclerView.adapter as BaseAdapter<Item>).showAd(layoutManager!!.findFirstVisibleItemPosition())
                                }

                                isFirst = true
                            }
                        },1000)

                    }
               // }

                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){

                    if(layoutManager!!.findLastVisibleItemPosition() != items.lastIndex && layoutManager!!.findLastVisibleItemPosition() >= ITEMS_VISIBLE_PER_PAGE - 1) {

                        (recyclerView.adapter as BaseAdapter<Item>).hideAd()
                    }

                    isFinallyStopped = false
                }

            }
        })


    }

    private fun addItems(){
        items!!.add(Item(R.drawable.apple,"20 января", 5))
        items.add(Item(R.drawable.beach,"18 января", 14))
        items.add(Item(R.drawable.butterfly,"16 января", 24))
        items.add(Item(R.drawable.cat,"15 января", 83))
        items.add(Item(R.drawable.heart,"14 января", 3))
        items.add(Item(R.drawable.lodki,"12 января", 0))
        items.add(Item(R.drawable.mouse,"11 января", 64))
        items.add(Item(R.drawable.oduvanchik,"11 января", 7))
        items.add(Item(R.drawable.planet,"10 января", 8))
        items.add(Item(R.drawable.spring,"8 января", 26))
    }


    companion object{

        const val ITEMS_VISIBLE_PER_PAGE = 2
    }

}
