package com.furymaxim.recyclerviewadproject

class AdapterItemsAreNullException(message: String): Exception(message)